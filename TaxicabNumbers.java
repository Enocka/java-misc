import java.util.PriorityQueue;

/**
 * A taxicab number is an integer that can be expressed as the sum of two cubes of
 * positive integers in two different ways: a^3 + b^3 = c^3 + d^3.
 * For example, 17291729 is the smallest taxicab number: 9^3 + 10^3 = 1^3 + 12^3.
 * Design an algorithm to find all  taxicab numbers with a, b, c, and d less than n.
 *
 * Version 1: Use time proportional to n^2 log n and space proportional to n^2.
 * Version 2: Use time proportional to n^2 log n and space proportional to n.
 * */

public class TaxicabNumbers {
    int n = 1000;
    public static void main(String[] args){
        TaxicabNumbers tn = new TaxicabNumbers();
        tn.run();
    }

    PriorityQueue<Integer> taxicabs = new PriorityQueue();

    public void run(){
        for(int i = 0; i < n; i++){
            for(int j = i; j < n; j++){
                taxicabs.add((i*i*i) + (j*j*j));
            }
        }
        int previousInt = taxicabs.poll();
        int nextInt;
        while (!taxicabs.isEmpty()){
            nextInt = taxicabs.poll();
            if (nextInt == previousInt)
                System.out.println("Found a taxicab number: "+nextInt);
            previousInt = nextInt;
        }
    }

}
