/*
Search in a bitonic array.
An array is bitonic if it is comprised of an increasing sequence of integers followed immediately by a decreasing sequence of integers.
Write a program that, given a bitonic array of n distinct integer values, determines whether a given integer is in the array.

Standard version: Use ∼3lgn compares in the worst case.
Signing bonus: Use ∼2lgn compares in the worst case (and prove that no algorithm can guarantee to perform fewer than ∼2lgn compares in the worst case).
* */
public class SearchBitonic {

    int searchResult = Integer.MAX_VALUE;

    public int searchBitonic(int value, int [] arr){
        int inflectionIndex = searchInflection(arr, 0, arr.length-1);
        System.out.println("inflectionIndex :"+inflectionIndex);
        binarySearch(value, arr, 0, inflectionIndex);
        if(searchResult == Integer.MAX_VALUE)
            return binarySearch(value, arr, inflectionIndex, arr.length-1);
        else return searchResult;
    }

    public int binarySearch(int val, int[] arr, int start, int end){
        int mid = (start+end)/2;
        if(start > end) return searchResult;
        System.out.println("start: "+start+" end: "+end);
        if(start == end)
            if(arr[start] == val)
                return arr[start];
        if(arr[mid] == val)
            return val;
        if(arr[mid] > val)
            binarySearch(val, arr, start, mid);
        else binarySearch(val, arr, mid+1, end);
        return searchResult;
    }

    //Returns the index of the inflection point, if doesnt exist returns Int.MAX_VALUE
    public int searchInflection(int[] arr, int start, int end){
        if(start == end) return Integer.MAX_VALUE;
        int mid = (start + end)/2;
        if(arr[mid-1] < arr[mid] && arr[mid] > arr[mid+1])
            return mid;
        if(arr[mid-1] < arr[mid] && arr[mid] < arr[mid+1])
            return searchInflection(arr, mid, end);
        else return searchInflection(arr, start, mid);
    }

    public static void main(String[] args){
        try{
            SearchBitonic sb = new SearchBitonic();
            sb.run(args);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void run(String[] args){
        int[] array = {1, 3, 5, 7, 12, 14, 15, 21, 32, 10, 6, 5, 3, 2, 0};
        int find = 14;
        int res = searchBitonic(find, array);
        System.out.println("res :: "+res);
    }
}
