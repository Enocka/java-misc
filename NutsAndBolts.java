/*
A disorganized carpenter has a mixed pile of n nuts and n bolts.
The goal is to find the corresponding pairs of nuts and bolts.
Each nut fits exactly one bolt and each bolt fits exactly one nut.
By fitting a nut and a bolt together, the carpenter can see which one is bigger (but the carpenter cannot compare two nuts or two bolts directly).
Design an algorithm for the problem that uses at most proportional to nlogn compares (probabilistically).
* */


public class NutsAndBolts {
    int[]nuts = {23, 24, 25, 31, 34, 35, 45, 54};
    int[]bolts = {35, 32, 34, 45, 23, 24, 25, 54};

    public static void main(String[] args){
        NutsAndBolts nb = new NutsAndBolts();
        nb.run(args);

    }

    public void run(String[] args){
        check(0, bolts.length-1);
        printArray("nuts: ", nuts);
        printArray("bolts ", bolts);
    }

    public void check(int start, int end){
        if (start >= end) return;
        int firstBoltsPosInSorted = partition(bolts, start, end);
//        System.out.println("posInBolts "+firstBoltsPosInSorted);
        int returnedBolt = bolts[firstBoltsPosInSorted];
        partition(nuts, start, end);
//        if (nuts[firstBoltsPosInSorted] != bolts[firstBoltsPosInSorted]) return;
        if (firstBoltsPosInSorted - 1 >= 0) {
//            System.out.println("Branch 2 fired");
            check(start, firstBoltsPosInSorted - 1);
        }
        if (firstBoltsPosInSorted + 1 < nuts.length) {
//            System.out.println("Branch 1 fired");
            check(firstBoltsPosInSorted+1, end);
        }
        if (nuts[firstBoltsPosInSorted] != bolts[firstBoltsPosInSorted]) {
            System.out.println("Missing nut for bolt "+bolts[firstBoltsPosInSorted]);
            return;
        }
    }

    public int partition(int[] arr, int start, int end){
        int i = start+1, j = end, pivot = arr[start];
        while (true){
//            System.out.println("pivot "+pivot);
//            System.out.println("i == j || (i >= arr.length || i < 0) || (j >= arr.length || j < 0) "+(i == j || (i >= arr.length || i < 0) || (j >= arr.length || j < 0)));
//            System.out.println("i "+i+" arr[i] "+arr[i]);
//            System.out.println("j "+j+" arr[j] "+arr[j]);
            if (i == j || (i >= arr.length || i < 0) || (j >= arr.length || j < 0)) {
                break;
            }
            if (arr[i] == arr[j]) {
                if (arr[i] < pivot) i++;
                if (arr[j] >= pivot)  j--;
                continue;
            }
            while (arr[i] < pivot){
//                System.out.println("arr[i] "+arr[i]+" smaller than pivot"+pivot);
                if (i == end) break;
                i++;
            }
//            System.out.println("Found larger than pivot "+arr[i]);
            while (arr[j] > pivot){
//                System.out.println("arr[j] "+arr[j]+" larger than pivot"+pivot);
                if (j == start) break;
                j--;
            }
//            System.out.println("Found smaller than pivot "+arr[j]);
//            if (i > j) return start;
            if (i > j) {
                break;
            }
            if (i < j) exch(arr, i, j);
//            printArray("After exchanging", arr);
//            System.out.println("i: "+i+" j: "+j);
        }
//        System.out.println("arr[start] "+arr[start]);
//        System.out.println("arr[j] "+arr[j]);
//        System.out.println("j: "+j);
//        System.out.println("start: "+start);
        if (j <= arr.length && j > 0)
            if (arr[start] > arr[j])
                exch(arr, start, j); //23 24 25 31 34 23 24 25 31 34 35 45 54
//        System.out.println("This is the returned position "+j);
        return j;
    }

    public void printArray(String title, int[] arr){
        System.out.println(title);
        for (int i = 0; i < arr.length; i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println();
    }

    public void exch(int[] arr, int i, int j){
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
