/*
Get the shortest range of car park slots with three cars
*/

public class SpacedCarPark {

    class Range{
        Integer left = null;
        Integer mid = null;
        Integer right = null;
        int getRange(){
            if (!isFull()) return 0;
            return right - left;
        }
        boolean isFull(){
            return left != null && mid != null && right != null;
        }

        @Override
        public String toString() {
            return "Range{" +
                    "left=" + left +
                    ", mid=" + mid +
                    ", right=" + right +
                    '}';
        }
    }

    public static void main(String[] args){
        SpacedCarPark scp = new SpacedCarPark();
        scp.run(args);
    }

    public void run(String[] args){
        int [] carPark = {0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1};
        Range shortestRange = getRange(carPark);
        if (shortestRange != null)
        {
            System.out.println("shortestRange");
            System.out.println(shortestRange.toString());
        }
    }

    public Range getRange(int [] arr){
        Range shortestRange = new Range();
        Range newRange = new Range();

        for (int i = 0; i < arr.length; i++){
            if(arr[i] == 1){
                if (!shortestRange.isFull()) {
                    addToIndexRange(shortestRange, i);
                    addToIndexRange(newRange, i);
                    System.out.println("shortestRange");
                    System.out.println(shortestRange.toString());
                    continue;
                } else {
                    newRange.left = shortestRange.mid;
                    newRange.mid = shortestRange.right;
                    newRange.right = i;
                }
                if (newRange.getRange() > shortestRange.getRange()){
                    shortestRange = newRange;
                }
            }
        }
        System.out.println("shortestRange");
        System.out.println(shortestRange.toString());
        if (shortestRange.isFull()) return shortestRange;
        else return null;
    }

    public void addToIndexRange(Range range, int index){
        if (range.right == null) {
            range.right = index;
            return;
        }
        if (range.mid == null) {
            range.mid = range.right;
            range.right = index;
            return;
        }
        range.left = range.mid;
        range.mid = range.right;
        range.right = index;
    }
}
