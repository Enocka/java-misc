/*
Egg drop. Suppose that you have an n-story building (with floors 1 through n) and plenty of eggs.
An egg breaks if it is dropped from floor T or higher and does not break otherwise.
Your goal is to devise a strategy to determine the value of T given the following limitations on the number of eggs and tosses:

Version 0: 1 egg, ≤ T tosses.
-Start from the bottom. Drop the egg in each subsequent floor until you find floor T.

Version 1: ∼1lgn eggs and ∼1lgn tosses.
0-If left with one egg, go sequentially from last safe floor (if no safe floor start with 0).
1-Start from the bottom. Drop the egg at floor 1, 2, 4, 8 ...
2-When the egg breaks, do 1, but start from the last floor that didn't break. e.g if it breaks at 32, start do 17, 18, 20, 28 ...

It should work if the lgn results in an extra toss from the decimal point.
If no extra toss:

or

0-If left with one egg, go sequentially from last safe floor (if no safe floor start with 0).
1-Start from the middle.
2-When egg breaks, got lower mid else higher mid.

Version 2: ~lgT eggs and ∼2lgT tosses.
0-If left with one egg, go sequentially from last safe floor (if no safe floor start with 0).
1-Start from the bottom. Drop the egg at floor 1, 2, 4, 8 ...
2-When the egg breaks, do 1, but start from the last floor that didn't break. e.g if it breaks at 32, start do 17, 18, 20, 28 ...


Version 3: 2 eggs and ~2sqrtn tosses.
0-If left with one egg, go sequentially from last safe floor (if no safe floor start with 0).
1-Start from the bottom. Drop the egg at floor 1, 2, 4, 8 ...
2-When the egg breaks, do 1, but start from the last floor that didn't break. e.g if it breaks at 32, start do 17, 18, 20, 28 ...

Version 4: 2 eggs and ≤ csqrtT tosses for some fixed constant c.
0-If left with one egg, go sequentially from last safe floor (if no safe floor start with 0).
1-Start from the bottom. Drop the egg at floor 1, 2, 4, 8 ...
2-When the egg breaks, do 1, but start from the last floor that didn't break. e.g if it breaks at 32, start do 17, 18, 20, 28 ...
* */