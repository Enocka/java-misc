import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayDeque;

public class GoodLifeParser {
    public class Category{
        public String name;
        public Category(String name){
            this.name = name;
        }
    }
    public class SubCategory{
        public String categoryName;
        public String name;
        public SubCategory(String categoryName,String name){
            this.categoryName = categoryName;
            this.name = name;
        }
    }
    public class Product{
        public String title;
        public String price;
        public Product(String title, String price){
            this.title = title;
            this.price = price;
        }
        public String toString() {
            return "Title: '" + this.title + "', Price: '" + this.price + "'";
        }
    }
    public static Category [] categories = new Category[4];
    public static SubCategory [] subCategories;

    public static void main(String[] args) throws IOException {
        try {
            GoodLifeParser glp = new GoodLifeParser();
            glp.run(args);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void run(String[] args) throws IOException {
        categories[0] = new Category("pharmacyH");
        categories[1] = new Category("beautyH");
        categories[2] = new Category("personalH");
        categories[3] = new Category("suppH");
        File file;
        Document doc;
        Elements elements;
        FileWriter writer;
//        File navbar = new File("/home/lobi/IdeaProjects/I/src/Navbar.html");
        file = new File("/home/lobi/gdlf/pharmacy/cough-cold-flu/cough-cold-flu.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");

        // 1.1 Cough-Cold-Flu subcategory (entails cold & flu, cough and sore throat subsubcategories)
        ArrayDeque<Product> CoughColdFluCategory =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            CoughColdFluCategory.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/non-categorised-cough-cold-flu.txt");
        for(Product p: CoughColdFluCategory) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();

//        Forever me a dea basi nipe kidogo song nimerithika sana unachonipa sana
        //arrwoy boy maryokun woman

        // 1.1.1 cold & flu subsubcategories
        file = new File("/home/lobi/gdlf/pharmacy/cough-cold-flu/cold&flu.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> ColdAndFluSubSub =  new ArrayDeque<>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            ColdAndFluSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/cough-cold-flu/cold-flu.txt");
        for(Product p: CoughColdFluCategory) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();


        // 1.1.2 cough subsubcategories
        file = new File("/home/lobi/gdlf/pharmacy/cough-cold-flu/cough.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> CoughSubSub =  new ArrayDeque<>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            CoughSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/cough-cold-flu/cough.txt");
        for(Product p: CoughSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();

        // 1.1.3 sore-throat subsubcategories
        file = new File("/home/lobi/gdlf/pharmacy/cough-cold-flu/sore-throat.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> SoreThroatSubSub =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            SoreThroatSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/cough-cold-flu/sore-throat.txt");
        for(Product p: SoreThroatSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();



        //1.2 Nutrition&Fitness subcategory (entails body builder, ultimate-sports-nutrition & nutrition subsubcategories)
        file = new File("/home/lobi/gdlf/pharmacy/nutrition&fitness/nutrition-and-fitness.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> NutritionAndFitnessCategory =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            NutritionAndFitnessCategory.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/non-categorised-nutrition-and-fitness.txt");
        for(Product p: NutritionAndFitnessCategory) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();


        // 1.2.1 body builder subsubcategory
        file = new File("/home/lobi/gdlf/pharmacy/nutrition&fitness/body-builder.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> BodyBuilderSubSub =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            BodyBuilderSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/nutrition&fitness/body-builder.txt");
        for(Product p: BodyBuilderSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();

        // 1.2.2 ultimate-sports-nutrition subsubcategory
        file = new File("/home/lobi/gdlf/pharmacy/nutrition&fitness/ultimate-sports-nutrition.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> UltimateSportsNutritionSubSub =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            UltimateSportsNutritionSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/nutrition&fitness/ultimate-sports-nutrition.txt");
        for(Product p: UltimateSportsNutritionSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();

        // 1.2.3 nutrition subsubcategory
        file = new File("/home/lobi/gdlf/pharmacy/nutrition&fitness/nutrition.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> NutritionSubSub =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            NutritionSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/nutrition&fitness/ultimate-sports-nutrition.txt");
        for(Product p: NutritionSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();


        //1.3 Eye&EarCare subcategory (entails eye-drops, ear-drops and accessories)
        file = new File("/home/lobi/gdlf/pharmacy/eye&ear-care/eye-and-ear-care.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> EyeAndEarCareCategory =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            EyeAndEarCareCategory.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/non-categorised-eye-and-ear-care.txt");
        for(Product p: EyeAndEarCareCategory) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();


        // 1.3.1 eye-drops subsubcategory
        file = new File("/home/lobi/gdlf/pharmacy/eye&ear-care/eye-drops.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> EyeDropsSubSub =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            EyeDropsSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/eye&ear-care/eye-drops.txt");
        for(Product p: EyeDropsSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();

        // 1.3.2 ear-drops subsubcategory
        file = new File("/home/lobi/gdlf/pharmacy/eye&ear-care/ear-drops.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> EarDropsSubSub =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            EarDropsSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/eye&ear-care/ear-drops.txt");
        for(Product p: EarDropsSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();

        // 1.3.3 accessories subsubcategory
        file = new File("/home/lobi/gdlf/pharmacy/eye&ear-care/accessories.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> AccessoriesSubSub =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            AccessoriesSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/eye&ear-care/accessories.txt");
        for(Product p: AccessoriesSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();

        //1.4 SexualHealthAndWellness subcategory (entails condoms, fertility-and-pregnancy, and lubricants-and-nassage-gel)
        file = new File("/home/lobi/gdlf/pharmacy/sexual-health-and-wellness/sexual-health-and-wellness.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> SexualHealthAndWellnessCategory =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            SexualHealthAndWellnessCategory.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/non-categorised-sexual-health-and-wellness.txt");
        for(Product p: SexualHealthAndWellnessCategory) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();


        // 1.4.1 condoms subsubcategory
        file = new File("/home/lobi/gdlf/pharmacy/sexual-health-and-wellness/condoms.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> CondomsSubSub =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            CondomsSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/sexual-health-and-wellness/condoms.txt");
        for(Product p: CondomsSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();

        // 1.4.2 fertility-and-pregnancy subsubcategory
        file = new File("/home/lobi/gdlf/pharmacy/sexual-health-and-wellness/fertility-and-pregnancy.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> FertAndPregSubSub =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            FertAndPregSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/sexual-health-and-wellness/fertility-and-pregnancy.txt");
        for(Product p: FertAndPregSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();

        // 1.4.3 lubricants&-and-pregnancy subsubcategory
        file = new File("/home/lobi/gdlf/pharmacy/sexual-health-and-wellness/lubricants-and-massage-gel.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> LubAndMassageGelSubSub =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            LubAndMassageGelSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/sexual-health-and-wellness/lubricants-and-massage-gel.txt");
        for(Product p: LubAndMassageGelSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();

        // 1.5 First Aid subcategory (entails first aid kits, antihistamine, antiseptic  subsubcategories)
        ArrayDeque<Product> FirstAidCategory =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            FirstAidCategory.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/non-categorised-first-aid.txt");
        for(Product p: FirstAidCategory) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();

//        Forever me a dea basi nipe kidogo song nimerithika sana unachonipa sana
        //arrwoy boy maryokun woman

        // 1.5.1 first aid kits subsubcategories
        file = new File("/home/lobi/gdlf/pharmacy/first-aid/first-aid-kits.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> FirstAidKitsSubSub =  new ArrayDeque<>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            FirstAidKitsSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/first-aid/first-aid-kits.txt");
        for(Product p: FirstAidKitsSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();


        // 1.5.2 antihistamine subsubcategories
        file = new File("/home/lobi/gdlf/pharmacy/first-aid/antihistamine.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> AntihistamineSubSub =  new ArrayDeque<>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            AntihistamineSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/first-aid/antihistamine.txt");
        for(Product p: AntihistamineSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();

        // 1.5.3 Antiseptic subsubcategories
        file = new File("/home/lobi/gdlf/pharmacy/first-aid/antiseptic.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> AntisepticSubSub =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            AntisepticSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/first-aid/antiseptic.txt");
        for(Product p: AntisepticSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();



        //1.6 GastroStomach subcategory (entails indigestion-heartburn-and-acidity, dewormers & digestive-health)
        file = new File("/home/lobi/gdlf/pharmacy/gastro-stomach/gastro-stomach.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> GastroStomachCategory =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            GastroStomachCategory.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/non-categorised-gastro-stomach.txt");
        for(Product p: GastroStomachCategory) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();


        // 1.6.1 indigestion-heartburn-acidity subsubcategory
        file = new File("/home/lobi/gdlf/pharmacy/gastro-stomach/indigestion-heartburn-acidity.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> IndigeHeartAcSubSub =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            IndigeHeartAcSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/gastro-stomach/indigestion-heartburn-acidity.txt");
        for(Product p: IndigeHeartAcSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();

        // 1.6.2 dewormers subsubcategory
        file = new File("/home/lobi/gdlf/pharmacy/gastro-stomach/dewormers.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> DewormersSubSub =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            DewormersSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/gastro-stomach/dewormers.txt");
        for(Product p: DewormersSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();

        // 1.6.3 Digestive Health subsubcategory
        file = new File("/home/lobi/gdlf/pharmacy/gastro-stomach/digestive-health.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> DigestiveHealthSubSub =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            DigestiveHealthSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/gastro-stomach/digestive-health.txt");
        for(Product p: DigestiveHealthSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();


        //1.7 HealthCareEquipment subcategory (entails blood-pressure-equipment, glucose monitors and strips and thermometers)
        file = new File("/home/lobi/gdlf/pharmacy/health-care-equipment/health-care-equipment.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> HealthCareEquipmentCategory =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            HealthCareEquipmentCategory.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/non-categorised-health-care-equipment.txt");
        for(Product p: HealthCareEquipmentCategory) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();


        // 1.7.1 BP monitors subsubcategory
        file = new File("/home/lobi/gdlf/pharmacy/health-care-equipment/blood-pressure-monitors-and-accessories.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> BPMonitorsAndAccessSubSub =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            BPMonitorsAndAccessSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/health-care-equipment/blood-pressure-monitors-and-accessories.txt");
        for(Product p: BPMonitorsAndAccessSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();

        // 1.7.2 glucose monitors and strips subsubcategory
        file = new File("/home/lobi/gdlf/pharmacy/health-care-equipment/glucose-monitors-and-strips.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> GlucoseMonitorsStripsSubSub =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            GlucoseMonitorsStripsSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/health-care-equipment/glucose-monitors-and-strips.txt");
        for(Product p: GlucoseMonitorsStripsSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();

        // 1.7.3 thermometers subsubcategory
        file = new File("/home/lobi/gdlf/pharmacy/health-care-equipment/thermometers.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> ThermometersSubSub =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            ThermometersSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/health-care-equipment/thermometer.txt");
        for(Product p: ThermometersSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
//        time is up before we know there is a spot for everyone  .... we'll survive through the ' ...enemies
        writer.close();

        //1.8 PainRelief subcategory (entails pain-and-fever, muscle-and-joint-pain, and dental)
        file = new File("/home/lobi/gdlf/pharmacy/pain-relief/pain-relief.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> PainReliefCategory =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            PainReliefCategory.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/non-categorised-pain-relief.txt");
        for(Product p: PainReliefCategory) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();


        // 1.8.1 pain-and-fever subsubcategory
        file = new File("/home/lobi/gdlf/pharmacy/pain-relief/pain-and-fever.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> PainAndFeverSubSub =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            PainAndFeverSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/pain-relief/pain-and-fever.txt");
        for(Product p: PainAndFeverSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();

        // 1.8.2 muscle-and-joint-pain subsubcategory
        file = new File("/home/lobi/gdlf/pharmacy/pain-relief/muscle-and-joint-pain.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> MuscleAndJointPainsSubSub =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            MuscleAndJointPainsSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/pain-relief/muscle-and-joint-pain.txt");
        for(Product p: MuscleAndJointPainsSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();

        // 1.8.3 dental subsubcategory
        file = new File("/home/lobi/gdlf/pharmacy/spain-relief/dental.html");
        doc = Jsoup.parse(file, "UTF-8", "https://www.goodlife.co.ke/");
        ArrayDeque<Product> DentalSubSub =  new ArrayDeque<Product>();
        elements = doc.body().getElementsByClass("product-content");
        for (Element element : elements) {
            DentalSubSub.add(createProduct(element));
        }
        writer = new FileWriter("/home/lobi/gdlf/data/pharmacy/pain-relief/dental.txt");
        for(Product p: DentalSubSub) {
            writer.write(p.toString() + System.lineSeparator());
        }
        writer.close();

    }

    public Product createProduct(Element element){
        System.out.println(element.getElementsByTag("a").text());
        String title = element.getElementsByTag("a").text();
        String amount = element.getElementsByClass("amount").last().text();
        System.out.println(amount);
        return new Product(title, amount);
    }

}
