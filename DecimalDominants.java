import java.util.HashSet;
import java.util.Set;

/*
Given an array with n keys, design an algorithm to find all values that occur more than n/10 times.
The expected running time of your algorithm should be linear.
* */
public class DecimalDominants {

//    int[] arr = {23, 31, 24, 24, 25, 31, 34, 35, 45, 54, 23, 24, 25, 31, 34};
    int[] arr = {25, 31, 34, 35, 45, 54, 23, 24, 25, 31, 34};
//    int[] arr = { 35, 45, 54, 23, 24, 25, 31, 34};
//    int[] arr = { 35, 45, 54, 23, 24, 25};
//    int[] arr = { 35, 45, 54, 23};
//    int[] arr = { 45, 54, 23};
//    int[] arr = { 45, 23, 54};
//    int[] arr = {23, 24, 24, 23, 24, 25};
//    int[] arr = {24, 24, 23, 24};
//    int[] arr = {24, 25, 25, 25};
//    int[] arr = {24, 23, 25, 23};

//    int[] arr = {23, 31, 24, 31, 34, 24, 25, 31, 34, 35, 45, 54, 23, 24, 25, 31, 34};
//    int[] arr = {23, 31, 24,  31, 34, 24, 25, 31, 34, 24};
//    int[] arr = {31, 24, 31, 34, 24, 25, 31, 34, 24};
//    int[] arr = {23, 31, 24, 24, 25, 24, 31, 31};
//    int[] arr = {31, 24, 24, 25, 24, 31, 31};
//    int[] arr = {31, 24, 24, 25, 24};
//    int[] arr = {31, 24, 24, 25, 24};
//    int[] arr = {24, 25};
//    int[] arr = {25, 24};
//    int[] arr = {31, 24, 25, 24};
    int recursionCount = 0;
    public static void main(String[] args) throws Exception {
        DecimalDominants dd = new DecimalDominants();
        dd.run(args);
    }

    public void run(String [] args) throws Exception {
        Set res = new HashSet();
        int k = 7;

        int step = (arr.length/k);
        for (int i = step; i < arr.length; i = i + step){
            int iThSmallest = iThSmallest(arr, 0, arr.length-1, i);
            int iStreak = getStreak(arr, i);
            System.out.println("i "+i+" iThSmallest "+iThSmallest+" kStreak "+iStreak);
            if (iStreak > step) res.add(iThSmallest);
        }
        System.out.println(res.toString());
        check(0, arr.length-1);
        printArray("Sorted array", arr);
    }

    public int getStreak(int[] arr, int k) throws Exception {
        int count = 1;
        int leftwards = k - 1, rightWards = k + 1;
        while (iThSmallest(arr, 0, arr.length-1, leftwards) == arr[k]) {
//            System.out.println("count: "+count);
            count++;
//            System.out.println("leftwards: "+leftwards);
//            System.out.println("Found copy so increase "+iThSmallest(arr, 0, arr.length-1, leftwards)+"'s count to "+count);
            leftwards--;
            if (leftwards < 0) break;
        }
//        System.out.println("Now searching rightwards");
        int iThSmallest = iThSmallest(arr, 0, arr.length-1, k+1);
//        System.out.println("i: "+i+"");
        while (iThSmallest == arr[k]) {
            if (rightWards == k+1)
                System.out.println("right one step tupate "+arr[k]+" tena "+iThSmallest);
            if (rightWards == k+2)
                System.out.println("right one step again tupate "+arr[k]+" tena "+iThSmallest);
//            System.out.println("count: "+count);
            count++;
//            System.out.println("rightWards: "+rightWards);
//            System.out.println("Found copy so increase "+iThSmallest(arr, 0, arr.length-1, rightWards)+"'s count to "+count);
            rightWards++;
            if (rightWards > arr.length) break;
            iThSmallest = iThSmallest(arr, 0, arr.length-1, rightWards);
        }
        return count;
    }

    public int iThSmallest(int[] arr, int start, int end, int k) throws Exception {
        if (start==end || start > k || k > end) return arr[start];
        recursionCount++;
        if (recursionCount > 500) {
            System.out.println("start: "+start);
            System.out.println("end: "+end);
            System.out.println("k: "+k);
            throw new Exception("StackOverflow");
        }
        int randomElement = arr[start];
        int randElPos = partition(arr, start, end);
//        System.out.println("randomElement: "+randomElement);
//        System.out.println("randomElement position (randElPos): "+randElPos);
        if (randElPos == k) return arr[randElPos];
        else if (randElPos > k) {
            if (randElPos - 1 >= 0)
                return iThSmallest(arr, start, randElPos - 1, k);
            else return iThSmallest(arr, start, randElPos, k);
        }
        else  {
            if (randElPos + 1 < arr.length)
                return iThSmallest(arr, randElPos + 1, end, k);
            else return iThSmallest(arr, start, randElPos, k);
        }
    }

    public void check(int start, int end){
        if (start >= end) return;
        int partitionPos = partition(arr, start, end);
//        System.out.println("partitionPos: "+partitionPos);
//        printArray("After partitioning",arr);
        if (partitionPos - 1 >= 0) {
//            System.out.println("Branch 2 fired");
            check(start, partitionPos - 1);
        }
        if (partitionPos + 1 < arr.length) {
//            System.out.println("Branch 1 fired");
            check(partitionPos+1, end);
        }
    }

    public int partition(int[] arr, int start, int end){
        int i = start+1, j = end, pivot = arr[start];
        while (true){
//            System.out.println("pivot "+pivot);
//            System.out.println("i == j || (i >= arr.length || i < 0) || (j >= arr.length || j < 0) "+(i == j || (i >= arr.length || i < 0) || (j >= arr.length || j < 0)));
//            System.out.println("i "+i+" arr[i] "+arr[i]);
//            System.out.println("j "+j+" arr[j] "+arr[j]);
            if (i == j || (i >= arr.length || i < 0) || (j >= arr.length || j < 0)) {
                break;
            }
            if (arr[i] == arr[j]) {
                if (arr[i] < pivot) i++;
                if (arr[j] >= pivot)  j--;
                continue;
            }
            while (arr[i] < pivot){
//                System.out.println("arr[i] "+arr[i]+" smaller than pivot"+pivot);
                if (i == end) break;
                i++;
            }
//            System.out.println("Found larger than pivot "+arr[i]);
            while (arr[j] > pivot){
//                System.out.println("arr[j] "+arr[j]+" larger than pivot"+pivot);
                if (j == start) break;
                j--;
            }
//            System.out.println("Found smaller than pivot "+arr[j]);
//            if (i > j) return start;
            if (i > j) {
                break;
            }
            if (i < j) exch(arr, i, j);
//            printArray("After exchanging", arr);
//            System.out.println("i: "+i+" j: "+j);
        }
//        System.out.println("arr[start] "+arr[start]);
//        System.out.println("arr[j] "+arr[j]);
//        System.out.println("j: "+j);
//        System.out.println("start: "+start);
        if (j <= arr.length && j > 0)
            if (arr[start] > arr[j])
                exch(arr, start, j); //23 24 25 31 34 23 24 25 31 34 35 45 54
//        System.out.println("This is the returned position "+j);
        return j;
    }

    public void exch(int[] arr, int i, int j){
//        System.out.println("exhcanging "+arr[i]+" with "+arr[j]);
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public void printArray(String text, int[] arr){
        System.out.println(text);
        for (int i = 0; i < arr.length; i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println();
    }
}