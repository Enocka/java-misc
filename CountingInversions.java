import java.util.Arrays;

/*
An inversion in an array a[] is a pair of entries a[i] and a[j] such that i < j but a[i] > a[j]
Given an array, design a linearithmic algorithm to count the number of inversions.
 */
public class CountingInversions {
    Integer[] array;
    Integer[] sorted;
    public static void main(String[] args){
        int[] arr = {1, 2, 5, 6, 5, 7, 2, 8, 9, 56, 4, 676, 3, 5656, 6, 7, 999};
//     inversions = {0, 0, 3, 4, 3, 4, 0, 4, 4, 4,  1,  3,  0,  3,   0, 0,  0};

//          index = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9,  10,11,  12,13,   14,15,16};
        int[] ar2 = {1, 2, 2, 3, 4, 5, 5, 6, 6, 7, 7, 8, 9, 56, 676, 999, 5656};
        try{
            CountingInversions ci = new CountingInversions();
            ci.countInversions(arr);
//            ci.manuallyCount(arr);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public int countInversions(int[] arr){
//        System.out.print("Counting inversions for :");
//        for(int i = 0 ; i < arr.length; i++){
//            System.out.print(arr[i]+" ");
//        }
//        System.out.println();
        if(arr.length == 1) return 0;
        int mid = arr.length/2;
        int [] left = new int[mid];
        int [] right = new int[arr.length - mid];
        System.arraycopy(arr, 0, left, 0, left.length);
        System.arraycopy(arr, left.length, right, 0, right.length);
        return countInversions(left) + countInversions(right) + merge(left, right, arr);
//        return merge(left, right);
    }

    public int merge(int[] left, int[] right, int[] sorted){
//        System.out.println("Merging arrays: ");
//        for(int i = 0 ; i < left.length; i++){
//            System.out.print(left[i]+", ");
//        }
//        System.out.print(" and ");
//        for(int i = 0 ; i < right.length; i++){
//            System.out.print(right[i]+", ");
//        }
//        System.out.println();

        int size = left.length + right.length;
        int leftCounter = 0;
        int leftSize = left.length;
        int rightCounter = 0;
        int inversions = 0;
        for(int i = 0; i < size; i++){
            if(leftCounter == left.length){
                sorted[i] = right[rightCounter];
                rightCounter++;
                continue;
            }
            else if (rightCounter == right.length){
                sorted[i] = left[leftCounter];
                leftCounter++;
                continue;
            }
//            System.out.println("right["+rightCounter+"]: "+ right[rightCounter] +" < left["+leftCounter+"]: "+ left[rightCounter] + " " +(right[rightCounter] < left[leftCounter]));
            if (right[rightCounter] < left[leftCounter] ){
                inversions = inversions + leftSize;
                sorted[i] = right[rightCounter];
                rightCounter++;

            } else {
                sorted[i] = left[leftCounter];
                leftCounter++;
                leftSize--;
            }

        }
//        System.out.println("merge inversions: "+inversions);
        return inversions;
    }
}