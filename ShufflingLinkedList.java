import java.util.ArrayDeque;
import java.util.Iterator;

/*
Given a singly-linked list containing n items, rearrange the items uniformly at random.
Your algorithm should consume a logarithmic (or constant) amount of extra memory and run in time proportional to nlogn in the worst case.
* */
public class ShufflingLinkedList {
    ArrayDeque<Integer> list = new ArrayDeque();

    public static void main(String[] args){
        ShufflingLinkedList sll = new ShufflingLinkedList();
        sll.run();
    }

    public void run(){
        list.add(34);
        list.add(12);
        list.add(4);
        list.add(4546);
        list.add(334);
        list.add(77);list.add(13);list.add(31);list.add(1);
        list.add(7);list.add(2);list.add(4);list.add(3);list.add(1);
        ArrayDeque res = shuffle(list);
        System.out.println("Finished shuffling");
        while (!res.isEmpty()){
            System.out.print(res.remove() + " ");
        }
    }

    public ArrayDeque shuffle(ArrayDeque list){

//        System.out.println("Processing list: ");
        ArrayDeque listCopy = new ArrayDeque();
        while (!list.isEmpty()){
            int temp = (int)list.remove();
            listCopy.add(temp);
            System.out.print(temp + " ");
        }
        System.out.println();
        while (!listCopy.isEmpty()){
            int temp = (int)listCopy.remove();
            list.add(temp);
        }
        int size = list.size();
        if(size == 1) {
//            System.out.println("Merged list is of size 1  is "+list.peek());
            return list;
        }
        ArrayDeque left = new ArrayDeque();
        ArrayDeque right = new ArrayDeque();
        Iterator listIterator = list.iterator();
        while (listIterator.hasNext()){
            if(left.size() < size/2) left.add(listIterator.next());
            else right.add(listIterator.next());
        }
        ArrayDeque sortedLeft = shuffle(left);
        ArrayDeque sortedRight = shuffle(right);
        ArrayDeque sortedList = new ArrayDeque();
        for (int i = 0; i < size; i++){
            double rand = Math.random();
            if(rand > 0.5){
                if (sortedLeft.size() > 0){
                    sortedList.add(sortedLeft.remove());
                }
                else {
                    sortedList.addAll(sortedRight);
                    break;
                }
            } else {
                if (sortedRight.size() > 0) {
                    sortedList.add(sortedRight.remove());
                }
                else {
                    sortedList.addAll(sortedLeft);
                    break;
                }
            }
        }
        return sortedList;
    }
}
