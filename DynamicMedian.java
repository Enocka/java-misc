/*
Design a data type that supports insert in logarithmic time, find-the-median in constant time, and remove-the-median in
logarithmic time. If the number of keys in the data type is even, find/remove the lower median.
* */

import java.util.Comparator;
import java.util.PriorityQueue;

public class DynamicMedian {

    PriorityQueue<Integer> smallHalf;
    PriorityQueue<Integer> bigHalf;
    public static void main(String[] args){
        DynamicMedian dm = new DynamicMedian();
        dm.run();
    }

    public void run(){
        int[] input = {1, 3, 23, 342, 45, 2342, 45, 23343, 2, 34545, 34, 1,344, 565, 56, 79999, 7, 9, 0 , 1, 1,1,12,1,12,13,15};
        MinFirst minFirstComparator = new MinFirst();
        MaxFirst maxFirstComparator = new MaxFirst();
        smallHalf = new PriorityQueue<Integer>(input.length, maxFirstComparator);
        bigHalf = new PriorityQueue<Integer>(input.length, minFirstComparator);
        for (int i = 0; i < input.length; i++){
            int nextInt = input[i];
            addNum(nextInt);
            if(true){
                System.out.println("Items: "+smallHalf.toString() + bigHalf.toString());
                System.out.println("Median: "+getMedian());
            }
        }
    }

    public void addNum(int nextInt){
        if(smallHalf.size() == 0 && bigHalf.size() == 0) {
            smallHalf.add(nextInt);
            return;
        }
        if (nextInt < smallHalf.peek()){
            smallHalf.add(nextInt);
            nextInt = smallHalf.poll();
        }
        if (bigHalf.size() > smallHalf.size()){
            int fromBigHalf = bigHalf.poll();
            if (nextInt < fromBigHalf){
                smallHalf.add(nextInt);
                bigHalf.add(fromBigHalf);
                return;
            } else {
                smallHalf.add(fromBigHalf);
                bigHalf.add(nextInt);
            }
        }
        else if (smallHalf.size() > bigHalf.size() ){
            int fromSmallHalf = smallHalf.poll();
            if (nextInt > fromSmallHalf){
                bigHalf.add(nextInt);
                smallHalf.add(fromSmallHalf);
            } else {
                bigHalf.add(fromSmallHalf);
                smallHalf.add(nextInt);
            }
        }
        else bigHalf.add(nextInt);
    }

    public double getMedian(){
        if(bigHalf.size() > smallHalf.size()){
            return bigHalf.peek();
        } else if (smallHalf.size() > bigHalf.size()){
            return smallHalf.peek();
        } else {
            return ((double)(smallHalf.peek() + bigHalf.peek()) / 2);
        }
    }

    public class MinFirst implements Comparator  {
        @Override
        public int compare(Object a, Object b) {
            if((int)a > (int) b) return 1;
            else if ((int)b > (int)a) return -1;
            else return 0;
        }
    }

    public class MaxFirst implements Comparator  {
        @Override
        public int compare(Object a, Object b) {
            if((Integer)a > (Integer) b) return -1;
            if ((Integer)b > (Integer)a) return 1;
            return 0;
        }
    }
}