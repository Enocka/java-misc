/*
Question 1
Queue with two stacks.
Implement a queue with two stacks so that each queue operations takes a constant amortized number of stack operations.


Answer has a bug with peek()
* */


import java.util.*;

public class Queue2Stacks<Kitu> implements Iterable<Kitu> {

    Stack<Kitu> stackOne;
    int stackOneSize = 1;
    int stackOneLimit = 2;
    int stackTwoSize = 1;
    int stackTwoLimit = 2;
    Iterator stackIterator;
    Stack<Kitu> stackTwo;
    Stack<Kitu> tempStack;

    public Queue2Stacks(){
        stackOne = new Stack();
        stackTwo = new Stack();
    }

    @Override
    public Iterator<Kitu> iterator() {
        return null;
    }

    public void enqueue(Kitu k){
        stackOne.push(k);
//        stackOneSize++;
        if(stackOne.size() + stackTwo.size() >= stackOneLimit){
            copyIntoStackTwo();
            stackOne = new Stack();
            stackOneLimit = stackOneLimit * 2;
        }
    }

    public Kitu  dequeue() throws Exception{
        if(stackTwo.isEmpty() && stackOne.isEmpty() ) throw new Exception("Empty queue");
        if(stackTwo.isEmpty() && !stackOne.isEmpty()) {
            System.out.println("ng'eeee");
            copyIntoStackTwo();
        }
        System.out.println("stackTwo.isEmpty() && !stackOne.isEmpty()"+stackTwo.isEmpty() +" " + !stackOne.isEmpty());
        return stackTwo.pop();
    }

    public void copyIntoStackTwo(){
        System.out.println("copyIntoStackTwo inacalliwa");
        if(stackTwo.isEmpty()){
            while (!stackOne.isEmpty()){
                stackTwo.push(stackOne.pop());
            }
        } else {
            tempStack = new Stack();
            while (!stackTwo.isEmpty()){
                tempStack.push(stackTwo.pop());
            }
            while (!stackOne.isEmpty()){
                stackTwo.push(stackOne.pop());
            }
            while (!tempStack.isEmpty()){
                stackTwo.push(tempStack.pop());
            }
        }
    }

    public Kitu peek() throws Exception{
        System.out.println("StackTwo size ni "+stackTwo.size());
        if(stackTwo.isEmpty() && stackOne.isEmpty() ) throw new Exception("Empty queue");
        if(stackTwo.isEmpty() && !stackOne.isEmpty()) {
            return stackOne.peek();
        }
        return stackTwo.peek();
    }

    public static void main (String [] args) throws Exception{
        try {
            Queue2Stacks q2 = new Queue2Stacks();
            q2.run(args);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void run (String [] args) throws Exception{
        Queue2Stacks q2 = new Queue2Stacks();

        System.out.println("Adding a");
        q2.enqueue('a');
        System.out.println("stackOne :"+q2.stackOne.toString());
        System.out.println("stackTwo :"+q2.stackTwo.toString());
        System.out.println();

        System.out.println("Adding b");
        q2.enqueue('b');
        System.out.println("stackOne :"+q2.stackOne.toString());
        System.out.println("stackTwo :"+q2.stackTwo.toString());
        System.out.println();

        System.out.println("Adding c");
        q2.enqueue('c');
        System.out.println("stackOne :"+q2.stackOne.toString());
        System.out.println("stackTwo :"+q2.stackTwo.toString());
        System.out.println();

        System.out.println("Adding d");
        q2.enqueue('d');
        System.out.println("stackOne :"+q2.stackOne.toString());
        System.out.println("stackTwo :"+q2.stackTwo.toString());
        System.out.println();

        System.out.println("Calling dequeue");
        q2.dequeue();
        System.out.println("stackOne :"+q2.stackOne.toString());
        System.out.println("stackTwo :"+q2.stackTwo.toString());
        System.out.println();

        System.out.println("Adding e");
        q2.enqueue('e');
        System.out.println("stackOne :"+q2.stackOne.toString());
        System.out.println("stackTwo :"+q2.stackTwo.toString());
        System.out.println();

        System.out.println("Adding f");
        q2.enqueue('f');
        System.out.println("stackOne :"+q2.stackOne.toString());
        System.out.println("stackTwo :"+q2.stackTwo.toString());
        System.out.println();

        System.out.println("Calling dequeue");
        q2.dequeue();
        System.out.println("stackOne :"+q2.stackOne.toString());
        System.out.println("stackTwo :"+q2.stackTwo.toString());
        System.out.println();

        System.out.println("Adding g");
        q2.enqueue('g');
        System.out.println("stackOne :"+q2.stackOne.toString());
        System.out.println("stackTwo :"+q2.stackTwo.toString());
        System.out.println();

        System.out.println("Calling dequeue");
        q2.dequeue();
        System.out.println("stackOne :"+q2.stackOne.toString());
        System.out.println("stackTwo :"+q2.stackTwo.toString());
        System.out.println();

        System.out.println("Calling dequeue");
        q2.dequeue();
        System.out.println("stackOne :"+q2.stackOne.toString());
        System.out.println("stackTwo :"+q2.stackTwo.toString());
        System.out.println();

        System.out.println("Calling dequeue");
        q2.dequeue();
        System.out.println("stackOne :"+q2.stackOne.toString());
        System.out.println("stackTwo :"+q2.stackTwo.toString());
        System.out.println();

        System.out.println("Calling dequeue");
        q2.dequeue();
        System.out.println("stackOne :"+q2.stackOne.toString());
        System.out.println("stackTwo :"+q2.stackTwo.toString());
        System.out.println();

        System.out.println("Calling dequeue");
        q2.dequeue();
        System.out.println("stackOne :"+q2.stackOne.toString());
        System.out.println("stackTwo :"+q2.stackTwo.toString());
        System.out.println();

        System.out.println("Calling dequeue");
        q2.dequeue();
        System.out.println("stackOne :"+q2.stackOne.toString());
        System.out.println("stackTwo :"+q2.stackTwo.toString());
        System.out.println();
    }
}