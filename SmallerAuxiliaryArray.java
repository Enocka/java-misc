/*
Question 1
Merging with smaller auxiliary array.
Suppose that the subarray a[0] to a[n−1] is sorted and the subarray a[n] to a[2∗n−1] is sorted.
How can you merge the two subarrays so that a[0] to a[2∗n−1] is sorted using an auxiliary array
of length n (instead of 2n)?
* */
public class SmallerAuxiliaryArray {

    public static int [] a;
    public static int [] aux;
    public static int firstHalfEmptyFrom;
    public static int secondHalfEmptyFrom;

    public static void mergeLargestsHalfToAux(){
        System.out.println("mergeLargestsHalfToAux called");
        firstHalfEmptyFrom = (a.length/2) - 1;
        secondHalfEmptyFrom = a.length-1;
        int low = 0, high = a.length - 1, mid = a.length/2;
        mid = mid - 1;
        for(int i = aux.length-1; i >= 0; i--){
            System.out.println("a[high]:"+a[high]+" a[mid]"+a[mid]);
            if(high < (a.length/2) -1) {
                int x = (a.length/2) -1;
                System.out.println("high("+high+") < (a.length/2) -1 ("+ x+")");
                aux[i] = a[mid];
                mid--;
            }
            else if(mid < 0){
                int x = (a.length/2) -1;
                System.out.println("mid("+mid+") < 0)");
                aux[i] = a[high];
                high--;
            }
            else if (a[high] > a[mid]) {
                aux[i] = a[high];
                high--;
            }
            else if (a[mid] > a[high]) {
                aux[i] = a[mid];
                mid--;
            }
        }
        System.out.println("Final form of auxilliary array ");
        for(int i = 0; i < aux.length; i++){
            System.out.print(aux[i]+" ");
        }
        firstHalfEmptyFrom = mid+1;
        secondHalfEmptyFrom = high-1;
    }

    public static void mergeToSecondHalf(){
        int secondHalfIndex = secondHalfEmptyFrom, firstHalfIndex = firstHalfEmptyFrom;
        for(int i = a.length-1; i > a.length/2-1; i--){
            if(firstHalfEmptyFrom < 0) {
                a[i] = a[secondHalfEmptyFrom];
                secondHalfEmptyFrom--;
            }
            else if(secondHalfEmptyFrom < a.length/2) {
                a[i] = a[firstHalfEmptyFrom];
                firstHalfEmptyFrom--;
            }
            else if(a[firstHalfEmptyFrom] > a[secondHalfEmptyFrom]) {
                a[i] = a[firstHalfEmptyFrom];
                firstHalfEmptyFrom--;
            }
            else if (a[firstHalfEmptyFrom] < a[secondHalfEmptyFrom]){
                a[i] = a[secondHalfEmptyFrom];
                secondHalfEmptyFrom--;
            }

        }
    }

    public static  void combineTwoSortedHalfs(){
        //From second half
        for (int i = 0; i < a.length/2; i++){
            a[i] = a[i+(a.length/2)];
        }
        //From auxilliary array
        for (int i = a.length/2; i < a.length; i++){
            a[i] = aux[i-(a.length/2)];
        }
    }

    public static void setA(int[] a) {
        SmallerAuxiliaryArray.a = a;
    }

    public static void main(String [] args){
        int[] a = {5, 34, 67, 234, 909, 23, 34, 500, 502, 908 };
//        int[] a = {1, 5, 34, 67, 234, 909, 11, 23, 34, 500, 502, 908 };
//        int[] a = {1, 12, 24, 355, 566, 666, 2344, 3450, 3451, 23, 34, 500, 502, 908, 909, 910, 911, 3440 };
        aux = new int[a.length/2];
        setA(a);
        mergeLargestsHalfToAux();
        mergeToSecondHalf();
        combineTwoSortedHalfs();
        System.out.println();
        for (int i = 0; i < a.length; i++){
            System.out.print(a[i]+" ");
        }

    }
}
